package com.exilesoft.service;

import com.exilesoft.model.Device;

import java.util.List;

public interface DeviceService {

    void addDevice(Device device);

    List<Device> getDevices();

    void removeDevice(String registrationId);
}
