package com.exilesoft.service;

import com.exilesoft.model.Notification;

/**
 * Created by shanuka on 12/21/16.
 */
public interface GCMNotificationService {
    void sendNotification(Notification notification);
}
