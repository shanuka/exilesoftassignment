package com.exilesoft.service;

import com.exilesoft.dao.DeviceDao;
import com.exilesoft.model.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("deviceService")
@Transactional
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceDao dao;

    @Override
    public void addDevice(Device device) {
        dao.addDevice(device);
    }

    @Override
    public List<Device> getDevices() {
        return dao.getDevices();
    }

    @Override
    public void removeDevice(String registrationId) {

    }
}
