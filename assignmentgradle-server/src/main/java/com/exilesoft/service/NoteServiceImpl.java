package com.exilesoft.service;

import com.exilesoft.dao.NoteDao;
import com.exilesoft.model.Note;
import com.exilesoft.model.Notification;
import com.exilesoft.model.NotificationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("noteService")
@Transactional
public class NoteServiceImpl implements NoteService {

    @Autowired
    private NoteDao dao;

    @Autowired
    GCMNotificationService gCMNotificationService;

    @Autowired
    Notification notification;

    @Autowired
    NotificationData notificationData;

    @Override
    public void saveNote(Note note) {
        String noteBody = note.getNote();
        if (noteBody.length() > 30) {
            notificationData.setBody(noteBody.substring(0, 30));
        } else {
            notificationData.setBody(noteBody);
        }
        notificationData.setTitle(note.getTitle());
        notification.setNotification(notificationData);
        gCMNotificationService.sendNotification(notification);
        dao.saveNote(note);
    }

    @Override
    public List<Note> getAllNotes() {
        return dao.getAllNotes();
    }

    @Override
    public void deleteNoteById(String id) {
        dao.deleteNoteById(id);
    }

    @Override
    public Note findById(String id) {
        return dao.findById(id);
    }

    @Override
    public void updateNote(Note note) {
        dao.updateNote(note);
    }

}
