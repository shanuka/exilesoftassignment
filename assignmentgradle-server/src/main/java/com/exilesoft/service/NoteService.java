package com.exilesoft.service;

import com.exilesoft.model.Note;

import java.util.List;

public interface NoteService {

    void saveNote(Note note);

    List<Note> getAllNotes();

    void deleteNoteById(String id);

    Note findById(String id);

    void updateNote(Note note);
}
