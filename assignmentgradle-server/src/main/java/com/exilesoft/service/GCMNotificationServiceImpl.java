package com.exilesoft.service;

import com.exilesoft.config.LoggerInterceptor;
import com.exilesoft.dao.DeviceDao;
import com.exilesoft.model.Device;
import com.exilesoft.model.Notification;
import com.exilesoft.model.PushResponce;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by shanuka on 12/21/16.
 */
@Service("gCMNotificationService")
@Transactional
public class GCMNotificationServiceImpl implements GCMNotificationService {

    private static final Logger LOG = LoggerFactory.getLogger(GCMNotificationServiceImpl.class);

    private static final int MAX_RETRIES = 3;

    private static final int MAX_MULTICAST_SIZE = 1000;

    private static final Executor THREAD_POOL = Executors.newFixedThreadPool(5);

    @Autowired
    DeviceDao deviceDAO;


    @Override
    public void sendNotification(Notification notification) {
        LOG.debug("sendNotification");
        final List<String> validDeviceIds = getValidDeviceIds();
        if (CollectionUtils.isEmpty(validDeviceIds)) {
            LOG.error("No valid devices to send notification");
            return;
        }
        //notification.setTo(validDeviceIds.get(0));
        notification.setRegistrationIds(validDeviceIds);
        sendSyncNotification(notification);

    }

    private void sendSyncNotification(Notification notification) {
        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();

        restTemplate.getMessageConverters().add(
                new FormHttpMessageConverter());

        restTemplate.getMessageConverters().add(
                new StringHttpMessageConverter());

        restTemplate.getMessageConverters().add(
                new MappingJackson2HttpMessageConverter());
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.set("Authorization", "key=AAAA2c0vMPo:APA91bFnIh3Mh7Sk3qy7Dl7eBO4tGs2DMndePsIqxZsh-QSGUwoEcapXrCZO-83lrWF9RUftsjX2EcKEvZDDWYYTXtUmJ4ZraaE0pcYuDoLSZzT_ldINkOEpUB5cXIrVmB-zSt7V6ALo1YZZLUMMLnDsvmMRLMGiRQ");

        HttpEntity<String> requestEntity = null;
        try {
            requestEntity = new HttpEntity<String>(
                    mapper.writeValueAsString(notification), requestHeaders);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        ResponseEntity<String> response = restTemplate.exchange("https://fcm.googleapis.com/fcm/send", HttpMethod.POST, requestEntity, String.class);
        LOG.error(response + response.getBody().toString());
        LOG.error(notification + notification.toString());
    }

    private List<String> getValidDeviceIds() {
        final List<Device> registeredDeviceIds = deviceDAO.getDevices();
        final List<String> validDeviceIds = new ArrayList<>();
        for (Device deviceIdInNotification : registeredDeviceIds) {
            validDeviceIds.add(deviceIdInNotification.getGcmID());
        }
        return validDeviceIds;
    }

//    private void sendSyncNotification(Notification notification, String deviceId) {
//        final Message message = createMessage(notification);
//        try {
//            Result result = gcmSender.send(message, deviceId, MAX_RETRIES);
//            final String messageId = result.getMessageId();
//            if (messageId == null) {
//                LOG.error("Unable to send message {} . {}", result, message);
//                throw new UnableToSendNotificationException(result.toString());
//            }
//            LOG.info("Message send successfully {}", result);
//        } catch (IOException e) {
//            LOG.error("Unable to send message to device id: {}", deviceId, e);
//            throw new UnableToSendNotificationException("Internal comunication error");
//        }
//    }

    private void sendAsyncNotification(Notification notification) {


    }


    private class NotificationSenderThread implements Runnable {


        private final List<String> deviceIdsToSend;

        NotificationSenderThread(Notification message, List<String> deviceIdsToSend) {
            this.deviceIdsToSend = deviceIdsToSend;
        }

        public void run() {
            RestTemplate restTemplate = new RestTemplate();

        }

    }
}
