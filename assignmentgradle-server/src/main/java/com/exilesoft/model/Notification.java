package com.exilesoft.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by shanuka on 12/21/16.
 */
@Component
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Notification {
    private String to;
    private List<String> registrationIds;
    NotificationData notification;

    public Notification() {
    }

    public List<String> getRegistrationIds() {
        return registrationIds;
    }

    public void setRegistrationIds(List<String> registrationIds) {
        this.registrationIds = registrationIds;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public NotificationData getNotification() {
        return notification;
    }

    public void setNotification(NotificationData notification) {
        this.notification = notification;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "to=" + to +
                ", notification=" + notification +
                '}';
    }
}

