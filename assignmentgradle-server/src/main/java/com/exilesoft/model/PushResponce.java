package com.exilesoft.model;

/**
 * Created by shanuka on 9/29/16.
 */

public class PushResponce {

    private String message_id;

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [message_id = " + message_id + "]";
    }
}
