package com.exilesoft.model;

import org.springframework.stereotype.Component;

/**
 * Created by shanuka on 12/21/16.
 */
@Component
public class NotificationData {

    private String title;

    private String body;

    public NotificationData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "NotificationData{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}

