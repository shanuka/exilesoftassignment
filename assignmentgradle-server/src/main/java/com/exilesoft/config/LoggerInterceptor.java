package com.exilesoft.config;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * Created by shanuka on 12/21/16.
 */
public class LoggerInterceptor implements ClientHttpRequestInterceptor {

    private static final String TAG = "Request";


    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body,
                                        ClientHttpRequestExecution execution) throws IOException {

        String test = new String(body).replace("\\", "");
        request.getHeaders().add("accept", "application/json");
        request.getHeaders().add("Content-Type", "application/json");

        request.getHeaders().add("authorization", "key=AAAA2c0vMPo:APA91bFnIh3Mh7Sk3qy7Dl7eBO4tGs2DMndePsIqxZsh-QSGUwoEcapXrCZO-83lrWF9RUftsjX2EcKEvZDDWYYTXtUmJ4ZraaE0pcYuDoLSZzT_ldINkOEpUB5cXIrVmB-zSt7V6ALo1YZZLUMMLnDsvmMRLMGiRQ");

        ClientHttpResponse response = execution.execute(request,
                removeLast(test).getBytes());

        return response;
    }

    public String removeLast(String str) {
        if (str.length() > 0 && str.charAt(str.length() - 1) == '\"') {
            str = str.substring(1, str.length() - 1);
        }

        return str;
    }
}
