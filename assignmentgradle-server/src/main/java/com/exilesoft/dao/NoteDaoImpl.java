package com.exilesoft.dao;

import com.exilesoft.model.Note;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("noteDao")
public class NoteDaoImpl extends AbstractDao<Integer, Note> implements NoteDao {

    @Override
    public void saveNote(Note note) {
        persist(note);
    }

    @Override
    @Transactional
    public List<Note> getAllNotes() {
        Criteria criteria = getSession().createCriteria(Note.class);
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    @Override
    public void deleteNoteById(String id) {

    }

    @Override
    public Note findById(String id) {
        return null;
    }

    @Override
    public void updateNote(Note note) {

    }


}