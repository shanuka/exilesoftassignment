package com.exilesoft.dao;

import com.exilesoft.model.Note;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface NoteDao {
    
    void saveNote(Note note);

    List<Note> getAllNotes();

    void deleteNoteById(String id);

    Note findById(String id);

    void updateNote(Note note);

}