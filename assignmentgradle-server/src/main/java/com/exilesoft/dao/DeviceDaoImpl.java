package com.exilesoft.dao;

import com.exilesoft.model.Device;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("deviceDao")
public class DeviceDaoImpl extends AbstractDao<Integer, Device> implements DeviceDao {

    @Override
    public void addDevice(Device device) {
        persist(device);
    }

    @Override
    public List<Device> getDevices() {
        Criteria criteria = getSession().createCriteria(Device.class);
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    @Override
    public void removeDevice(String registrationId) {

    }

}