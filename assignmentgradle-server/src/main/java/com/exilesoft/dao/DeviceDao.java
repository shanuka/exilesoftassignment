package com.exilesoft.dao;

import com.exilesoft.model.Device;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DeviceDao {


    void addDevice(Device device);

    List<Device> getDevices();

    void removeDevice(String registrationId);

}