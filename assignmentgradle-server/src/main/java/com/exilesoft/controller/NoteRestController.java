package com.exilesoft.controller;

import com.exilesoft.model.MetaData;
import com.exilesoft.model.Note;
import com.exilesoft.service.NoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class NoteRestController {

    static final Logger logger = LoggerFactory.getLogger(NoteRestController.class);

    @Autowired
    private NoteService noteService;

    @Autowired
    MetaData metaData;


    @GetMapping("/notes")
    public List getTest() {
        return noteService.getAllNotes();
    }


    @PostMapping(value = "/notes")
    public ResponseEntity createNote(@RequestBody Note note) {

        noteService.saveNote(note);

        metaData.setMessage("Note added");
        metaData.setStatus(true);

        return new ResponseEntity(metaData, HttpStatus.OK);
    }


}