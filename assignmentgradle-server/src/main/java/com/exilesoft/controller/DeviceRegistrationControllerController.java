package com.exilesoft.controller;

import com.exilesoft.model.Device;
import com.exilesoft.model.MetaData;
import com.exilesoft.service.DeviceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class DeviceRegistrationControllerController {

    static final Logger logger = LoggerFactory.getLogger(DeviceRegistrationControllerController.class);

    @Autowired
    private DeviceService deviceService;

    @Autowired
    MetaData metaData;


    @GetMapping("/devices")
    public List getTest() {
        return deviceService.getDevices();
    }


    @PostMapping(value = "/devices")
    public ResponseEntity addeDevice(@RequestBody Device device) {

        deviceService.addDevice(device);

        metaData.setMessage("Device added");
        metaData.setStatus(true);

        return new ResponseEntity(metaData, HttpStatus.OK);
    }

//
//	@DeleteMapping("/customers/{id}")
//	public ResponseEntity deleteCustomer(@PathVariable Long id) {
//
//		if (null == customerDAO.delete(id)) {
//			return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
//		}
//
//		return new ResponseEntity(id, HttpStatus.OK);
//
//	}
//
//	@PutMapping("/customers/{id}")
//	public ResponseEntity updateCustomer(@PathVariable Long id, @RequestBody Customer customer) {
//
//		customer = customerDAO.update(id, customer);
//
//		if (null == customer) {
//			return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
//		}
//
//		return new ResponseEntity(customer, HttpStatus.OK);
//	}

}