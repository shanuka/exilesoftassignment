package com.exile.androidassesment;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.exile.androidassesment.clients.AddNoteRequest;
import com.exile.androidassesment.dto.NoteDto;
import com.exile.androidassesment.fragments.ProgressFragment;
import com.exile.androidassesment.models.MetaData;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.Date;
import java.util.List;

public class AddNoteActivity extends BaseActivity {

    private String lastRequestCacheKey;
    private AppCompatButton appCompatButtonNote;
    @NotEmpty
    private EditText editTextTitle;
    @NotEmpty
    private EditText editTextNote;

    private Validator validator;
    private TextInputLayout textInputLayoutTitle;
    private TextInputLayout textInputLayoutNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressFragment = ProgressFragment.newInstance();
        validator = new Validator(this);
        appCompatButtonNote = (AppCompatButton) findViewById(R.id.appCompatButtonNote);
        editTextTitle = (EditText) findViewById(R.id.editTextTitle);
        editTextNote = (EditText) findViewById(R.id.editTextNote);
        textInputLayoutTitle = (TextInputLayout) findViewById(R.id.textInputLayoutTitle);
        textInputLayoutNote = (TextInputLayout) findViewById(R.id.textInputLayoutNote);
        appCompatButtonNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });

        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                NoteDto mNoteDto = new NoteDto();
                mNoteDto.setDateOfCreated(new Date());
                mNoteDto.setNote(editTextNote.getText().toString());
                mNoteDto.setTitle(editTextTitle.getText().toString());
                performRequest(mNoteDto);
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(AddNoteActivity.this);

                    // Display error messages ;)
                    if (view instanceof EditText) {
                        if (view.getId() == R.id.editTextTitle) {
                            textInputLayoutTitle.setError(message);
                        }
                        if (view.getId() == R.id.editTextNote) {
                            textInputLayoutNote.setError(message);
                        }
                    } else {
                        Toast.makeText(AddNoteActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


    }

    public void performRequest(NoteDto mNoteDto) {
        showProgressFragment(R.id.content_main);
        AddNoteRequest request = new AddNoteRequest(
                "AddNoteRequest", mNoteDto);

        lastRequestCacheKey = request.createCacheKey();
        spiceManager.execute(request, lastRequestCacheKey,
                DurationInMillis.ALWAYS_EXPIRED, new AddNoteRequestListener());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                onBackPressed();
                break;
        }
        return true;
    }

    private class AddNoteRequestListener implements RequestListener<MetaData> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            System.out.println(spiceException);
        }

        @Override
        public void onRequestSuccess(MetaData notes) {
            removeProgressFragment();
            finish();
        }
    }
}
