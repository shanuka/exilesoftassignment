package com.exile.androidassesment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.transition.Visibility;
import android.view.View;

import com.exile.androidassesment.adapters.NodeAdapter;
import com.exile.androidassesment.clients.NoteListRequest;
import com.exile.androidassesment.fragments.ProgressFragment;
import com.exile.androidassesment.models.Note;
import com.exile.androidassesment.utils.TransitionHelper;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends BaseActivity {

    private String lastRequestCacheKey;
    private RecyclerView recyclerViewNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSelectRegistrationActivity();
            }
        });
        recyclerViewNote = (RecyclerView) findViewById(R.id.recyclerViewNote);
        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL); // (int spanCount, int orientation)
        recyclerViewNote.setLayoutManager(mLayoutManager);
        progressFragment = ProgressFragment.newInstance();
        performRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void performRequest() {
        showProgressFragment(R.id.content_main);
        NoteListRequest request = new NoteListRequest(
                "NoteListRequest");

        lastRequestCacheKey = request.createCacheKey();
        spiceManager.execute(request, lastRequestCacheKey,
                DurationInMillis.ALWAYS_EXPIRED, new NoteListRequestListener());
    }


    private void startSelectRegistrationActivity() {

        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(MainActivity.this, true);
        startAddNoteActivity(AddNoteActivity.class, pairs);
    }

    private void startAddNoteActivity(Class target, Pair<View, String>[] pairs) {
        Intent i = new Intent(MainActivity.this, target);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, pairs);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Visibility returnTransition = buildReturnTransition();
            getWindow().setReturnTransition(returnTransition);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            //
            startActivity(i, transitionActivityOptions.toBundle());

        } else {
            startActivity(new Intent(MainActivity.this, target));
            overridePendingTransition(
                    R.animator.push_right_in, R.animator.push_right_out);

        }
    }

    private class NoteListRequestListener implements RequestListener<Note[]> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            System.out.println(spiceException);
        }

        @Override
        public void onRequestSuccess(Note[] notes) {
            removeProgressFragment();

            NodeAdapter mNodeAdapter = new NodeAdapter(MainActivity.this, new ArrayList<Note>(Arrays.asList(notes)));
            recyclerViewNote.setAdapter(mNodeAdapter);

        }
    }


}
