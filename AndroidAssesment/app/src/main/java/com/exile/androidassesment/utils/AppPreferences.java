/*
 * Copyright (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.exile.androidassesment.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.os.Build;

/**
 * The type App preferences.
 */
public final class AppPreferences {
    /**
     * The constant KEY_FIST_APP.
     */
    public static final String KEY_FIST_APP = "firstApp";
    /**
     * The constant USER_NAME.
     */
    public static final String USER_NAME = "name";
    /**
     * The constant PASSWORD.
     */
    public static final String PASSWORD = "password";
    /**
     * The constant ACCESS_TOKEN.
     */
    public static final String ACCESS_TOKEN = "token";
    private static final String APP_SHARED_PREFS = AppPreferences.class
            .getSimpleName(); // Name of the file -.xml
    private static final String CONTACT_ID = "contactId";
    private final Context context;
    private SharedPreferences sharedPrefs;
    private Editor prefsEditor;

    /**
     * Instantiates a new App preferences.
     *
     * @param context the context
     */
    public AppPreferences(Context context) {
        this.context = context;
        this.sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS,
                Activity.MODE_PRIVATE);
        this.prefsEditor = sharedPrefs.edit();
    }

    /**
     * Is fist app boolean.
     *
     * @return the boolean
     */
    public Boolean isFistApp() {
        return sharedPrefs.getBoolean(KEY_FIST_APP, true);
    }

    /**
     * Put fist app.
     *
     * @param fist the fist
     */
    public void putFistApp(Boolean fist) {
        prefsEditor.putBoolean(KEY_FIST_APP, fist);
        prefsEditor.commit();
    }

    /**
     * Put user name.
     *
     * @param userName the user name
     */
    public void putUserName(String userName) {
        prefsEditor.putString(USER_NAME, userName);
        prefsEditor.commit();
    }

    /**
     * Gets user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return sharedPrefs.getString(USER_NAME, null);

    }

    /**
     * Put password.
     *
     * @param password the password
     */
    public void putPassword(String password) {
        prefsEditor.putString(PASSWORD, password);
        prefsEditor.commit();
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return sharedPrefs.getString(PASSWORD, null);

    }

    /**
     * Put access token.
     *
     * @param accesstoken the accesstoken
     */
    public void putAccessToken(String accesstoken) {
        prefsEditor.putString(ACCESS_TOKEN, accesstoken);
        prefsEditor.commit();
    }

    /**
     * Gets access token.
     *
     * @return the access token
     */
    public String getAccessToken() {
        return sharedPrefs.getString(ACCESS_TOKEN, null);

    }

    /**
     * Put contact id.
     *
     * @param contactId the contact id
     */
    public void putContactId(String contactId) {
        prefsEditor.putString(CONTACT_ID, contactId);
        prefsEditor.commit();
    }

    /**
     * Gets contact id.
     *
     * @return the contact id
     */
    public String getContactId() {
        return sharedPrefs.getString(CONTACT_ID, null);

    }

    /**
     * Remove access token.
     */
    public void removeAccessToken() {
        // return sharedPrefs.putString(ACCESS_TOKEN, "");

        prefsEditor.putString(ACCESS_TOKEN, null);
        prefsEditor.commit();

    }

    /**
     * Is empty access token boolean.
     *
     * @return the boolean
     */
    public Boolean isEmptyAccessToken() {
        if (getAccessToken() != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * method that will return whether the permission is accepted. By default it is true if the user is using a device below
     * version 23
     *
     * @param permission the permission
     * @return boolean
     */
    public boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            return (context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
        }
        return true;
    }

    /**
     * Just a check to see if we have marshmallows (version 23)
     *
     * @return boolean
     */
    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     *
     * @param permission the permission
     * @return boolean
     */
    public boolean shouldWeAsk(String permission) {
        return (sharedPrefs.getBoolean(permission, true));
    }

    /**
     * we will save that we have already asked the user
     *
     * @param permission the permission
     */
    public void markAsAsked(String permission) {
        prefsEditor.putBoolean(permission, false).apply();
    }
}