package com.exile.androidassesment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.exile.androidassesment.R;
import com.exile.androidassesment.models.Note;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shanuka on 12/16/16.
 */

public class NodeAdapter extends RecyclerView.Adapter<NodeAdapter.ViewHolder> {

    private final ArrayList<Note> mNoteList;
    private final Context mContext;

    private SparseBooleanArray selectedItems;

    // Provide a suitable constructor (depends on the kind of dataset)
    public NodeAdapter(Context mContext, ArrayList<Note> mNote) {
        this.mContext = mContext;
        mNoteList = mNote;
        selectedItems = new SparseBooleanArray();
    }

    public Note getItem(int currPos) {
        return mNoteList.get(currPos);
    }

    public void removeAllData() {
        mNoteList.clear();
        notifyDataSetChanged();
    }

    /**
     * Removes the item that currently is at the passed in position from the
     * underlying data set.
     *
     * @param position The index of the item to remove.
     */
    public void removeData(int position) {
        mNoteList.remove(position);
        notifyItemRemoved(position);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NodeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_note, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Note mNote = mNoteList.get(position);
        holder.textViewNote.setText(mNote.getNote());
        holder.textViewTitle.setText(mNote.getTitle());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        holder.textViewDate.setText(df.format(mNote.getDateOfCreated()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mNoteList.size();
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView textViewNote, textViewDate, textViewTitle;

        public ViewHolder(View v) {
            super(v);

            textViewNote = (TextView) v.findViewById(R.id.textViewNote);

            textViewDate = (TextView) v.findViewById(R.id.textViewDate);

            textViewTitle = (TextView) v.findViewById(R.id.textViewTitle);
        }
    }
}