package com.exile.androidassesment.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class NoteDto implements Serializable {


    private String title;

    private String note;

    //2016-12-08
    // @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dateOfCreated;

    public NoteDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDateOfCreated() {
        return dateOfCreated;
    }

    public void setDateOfCreated(Date dateOfCreated) {
        this.dateOfCreated = dateOfCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NoteDto noteDto = (NoteDto) o;

        if (title != null ? !title.equals(noteDto.title) : noteDto.title != null) return false;
        if (note != null ? !note.equals(noteDto.note) : noteDto.note != null) return false;
        return dateOfCreated != null ? dateOfCreated.equals(noteDto.dateOfCreated) : noteDto.dateOfCreated == null;

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (dateOfCreated != null ? dateOfCreated.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NoteDto{" +
                "title='" + title + '\'' +
                ", note='" + note + '\'' +
                ", dateOfCreated=" + dateOfCreated +
                '}';
    }
}