package com.exile.androidassesment.clients;

/**
 * Created by shanuka on 9/29/16.
 */

import android.util.Log;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;


/**
 * @author Shanuka Gayashan
 */
public class LoggerInterceptor implements ClientHttpRequestInterceptor {

    private static final String TAG = "Request";

    public LoggerInterceptor() {
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body,
                                        ClientHttpRequestExecution execution) throws IOException {

        String bodyString = new String(body).replace("\\", "");
        request.getHeaders().add("accept", "application/json");
        ClientHttpResponse response = execution.execute(request,
                removeLast(bodyString).getBytes());
        return response;
    }

    public String removeLast(String str) {
        if (str.length() > 0 && str.charAt(str.length() - 1) == '\"') {
            str = str.substring(1, str.length() - 1);
        }
        return str;
    }
}

