package com.exile.androidassesment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.transition.Visibility;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.exile.androidassesment.fragments.ProgressFragment;
import com.octo.android.robospice.Jackson2SpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * The type Base activity.
 */
public class BaseActivity extends AppCompatActivity {

    /**
     * The Progress fragment.
     */
    public ProgressFragment progressFragment;
    /**
     * The Spice manager.
     */
    protected SpiceManager spiceManager = new SpiceManager(
            Jackson2SpringAndroidSpiceService.class);

    /**
     * Instantiates a new Base activity.
     */
    public BaseActivity() {
        super();
    }

    /**
     * Hide keyboard.
     *
     * @param activity the activity
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Attach base context.
     *
     * @param newBase the new base
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * On start.
     */
    @Override
    public void onStart() {
        super.onStart();
        if (!spiceManager.isStarted()) {
            spiceManager.start(this);
        }

    }

    /**
     * On stop.
     */
    @Override
    public void onStop() {

        if (spiceManager.isStarted()) {
            spiceManager.shouldStop();
        }
        super.onStop();

    }

    /**
     * Remove progress fragment.
     */
    void removeProgressFragment() {
        if (progressFragment.isAdded()) {
            FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();

            fragmenttransaction.remove(progressFragment);
            fragmenttransaction.commit();
        }
    }

    /**
     * Show progress fragment.
     *
     * @param id the id
     */
    void showProgressFragment(int id) {
        if (!progressFragment.isAdded()) {
            FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();

            fragmenttransaction.add(id, progressFragment);

            fragmenttransaction.commit();
        } else {
            Log.d("progressFragment", "progressFragment else");
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Visibility buildReturnTransition() {
        Visibility enterTransition = new Slide();
        enterTransition.setDuration(300);
        return enterTransition;
    }
}
