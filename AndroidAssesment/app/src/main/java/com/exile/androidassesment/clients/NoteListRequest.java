package com.exile.androidassesment.clients;

import android.util.Log;

import com.exile.androidassesment.models.Note;
import com.exile.androidassesment.utils.Constant;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.client.HttpServerErrorException;

import java.util.ArrayList;
import java.util.List;


public class NoteListRequest extends SpringAndroidSpiceRequest<Note[]> {


    public String key;

    public NoteListRequest(String key) {
        super(Note[].class);
        this.key = key;
    }

    @Override
    public Note[] loadDataFromNetwork() throws Exception {

        String url = Constant.BASE_URL;
        String pathTemplate;
        pathTemplate = url
                + "notes";


        getRestTemplate().getMessageConverters().add(
                new FormHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);


        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        LoggerInterceptor loggerInterceptor = new LoggerInterceptor();
        interceptors.add(loggerInterceptor);
        getRestTemplate().setInterceptors(interceptors);


        try {
            // System.out.println("rw "+mNoteDto.getRequestBody());
            return getRestTemplate().getForObject(pathTemplate,
                    Note[].class);
        } catch (HttpServerErrorException e) {

            if (Constant.DEBUG_STATE) {
                // Log.d(TAG, "authString authorization" + authString);
                Log.d("Exception", "body " + e.getResponseBodyAsString());
            }
            throw e;
        } catch (Exception e) {

            throw e;

        }

    }

    public String createCacheKey() {
        return key;
    }
}
